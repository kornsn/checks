'use strict';

/* Controllers */

var phonecatControllers = angular.module('phonecatControllers', []);

phonecatControllers.controller('PhoneListCtrl', ['$scope', 'Phone',
    function ($scope, Phone) {
        $scope.phones = Phone.query();
        $scope.orderProp = 'age';
    }]);

phonecatControllers.controller('PhoneDetailCtrl', ['$scope', '$routeParams', 'Phone',
    function ($scope, $routeParams, Phone) {
        $scope.phone = Phone.get({phoneId: $routeParams.phoneId}, function (phone) {
            $scope.mainImageUrl = phone.images[0];
        });

        $scope.setImage = function (imageUrl) {
            $scope.mainImageUrl = imageUrl;
        };
    }]);

phonecatControllers.controller('CheckListCtrl', ['$scope', 'Bill', 'Payee',
    function ($scope, Bill, Payee) {
        $scope.current_bill = new Bill({items: []});

        $scope.update = function () {
            Bill.query({}, function (result) {
                $scope.bills = result;
            })
        };
        $scope.payees = Payee.query({}, function (payees) {
            var lookup = {};
            for (var i = 0, len = payees.length; i < len; i++) {
                var payee = payees[i];
                lookup[payee.id] = payee;
            }
            $scope.payees_lookup = lookup;
        });
        $scope.centralDate = new Date();
        $scope.update();

        $scope.select_bill = function (bill) {
            console.log("current bill: ", bill);
            $scope.current_bill = bill;
        };

        $scope.submit = function () {
            var bill = $scope.current_bill;
            if (bill.id != undefined) {
                bill.$update(function (user, putResponseHeader) {
                    $scope.update();
                })
            } else {
                bill.$save(function (user, putResponseHeader) {
                    $scope.update();
                })
            }
        };
    }
]);

phonecatControllers.constant('AUTH_EVENTS', {
        loginSuccess: 'auth-login-success',
        loginFailed: 'auth-login-failed',
        logoutSuccess: 'auth-logout-success',
        sessionTimeout: 'auth-session-timeout',
        notAuthenticated: 'auth-not-authenticated',
        notAuthorized: 'auth-not-authorized'
    })
    .constant('USER_ROLES', {
        all: '*',
        admin: 'admin',
        editor: 'editor',
        guest: 'guest'
    });

phonecatControllers.controller('LoginController', function ($scope, $rootScope, AUTH_EVENTS, AuthService) {
    $scope.credentials = {
        username: '',
        password: ''
    };
    $scope.login = function (credentials) {
        AuthService.login(credentials).then(function (user) {
            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
            $scope.setCurrentUser(user);
        }, function () {
            $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
        });
    };
});

phonecatControllers.controller('ApplicationController', function ($scope,
                                                                  USER_ROLES,
                                                                  AuthService) {
    $scope.currentUser = null;
    $scope.userRoles = USER_ROLES;
    $scope.isAuthorized = AuthService.isAuthorized;

    $scope.setCurrentUser = function (user) {
        $scope.currentUser = user;
    };
});

