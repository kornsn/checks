'use strict';

/* App Module */

var app = angular.module('app', [
    'ngRoute',
    'ngResource',
    'ngCookies',
    'phonecatControllers',
    'phonecatFilters',
    'phonecatServices',
    'phonecatDirectives'
]);
//
// app.config(['$httpProvider', function($httpProvider) {
//     $httpProvider.defaults.xsrfCookieName = 'csrftoken'
//     $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken'
// }]);

app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider
            .when('/login', {
                templateUrl: 'partials/login-form.html',
                controller: 'LoginController'
            })
            .when('/checks', {
                templateUrl: 'partials/checks.html',
                controller: 'CheckListCtrl'
            }).
        otherwise({
            redirectTo: '/checks'
        });
    }]);

// app.
// config(['$httpProvider', function ($httpProvider) {
//     // django and angular both support csrf tokens. This tells
//     // angular which cookie to add to what header.
//     $httpProvider.defaults.xsrfCookieName = 'csrftoken';
//     $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
// }]);

app.config(function ($httpProvider) {
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    })
   .run( function run( $http, $cookies ){
         $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
})
// app.run(function($cookieStore, $rootScope, $http, $cookies) {
//     // if ($cookieStore.get('djangotoken')) {
//     //     $http.defaults.headers.common['Authorization'] = 'Token ' + $cookieStore.get('djangotoken');
//     //     document.getElementById("main").style.display = "block";
//     // } else {
//     //     document.getElementById("login-holder").style.display = "block";
//     // }
//     // // titleService.setSuffix( '[title]' );

//     // For CSRF token compatibility with Django
//     $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
// });

