config = {
	backend: {
		url: '/'
	},
	resources: {
		Payee: {
			endpoint: 'payees/'
		},
		Bill: {
			endpoint: 'bills/'
		},
		Tag: {
			endpoint: 'tags/'
		}
	}
};

moment.locale('ru');