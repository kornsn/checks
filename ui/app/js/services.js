'use strict';

/* Services */

var services = angular.module('phonecatServices', ['ngResource']);

var resourceObjects = config.resources;

// for (var resource_name in resourceObjects) {
//     if (resourceObjects.hasOwnProperty(resource_name)) {
//         var resource = resourceObjects[resource_name];
//         addResource(services, resource_name, resourceObjects[resource_name].endpoint)
//     }
// }

// function addResource(services, resource_name, endpoint) {
//     services.factory(resource_name, ['$resource',
//         function ($resource) {
//             return $resource(config.backend.url + endpoint + ':id')
//         }
//         ])
// }

services.factory('AuthService', function ($http, Session) {
    var authService = {};

    authService.login = function (credentials) {
        credentials.next = '/';
        credentials.submit = 'Log in'
        console.log(credentials)
        return $http({
            method: 'POST',
            url: config.backend.url + 'api-auth/login/',
            data: $.param(credentials),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(function (res) {
            Session.create(res.data.id, res.data.user.id,
                res.data.user.role);
            return res.data.user;
        });
    };

    authService.isAuthenticated = function () {
        return !!Session.userId;
    };

    authService.isAuthorized = function (authorizedRoles) {
        if (!angular.isArray(authorizedRoles)) {
            authorizedRoles = [authorizedRoles];
        }
        return (authService.isAuthenticated() &&
            authorizedRoles.indexOf(Session.userRole) !== -1);
    };

    return authService;
})
.service('Session', function () {
    this.create = function (sessionId, userId, userRole) {
        this.id = sessionId;
        this.userId = userId;
        this.userRole = userRole;
    };
    this.destroy = function () {
        this.id = null;
        this.userId = null;
        this.userRole = null;
    };
});


services.factory('Phone', ['$resource',
   function ($resource) {
       return $resource('phones/:phoneId.json', {}, {
           query: {method: 'GET', params: {phoneId: 'phones'}, isArray: true}
       });
   }]);

services.factory('Bill', ['$resource',
   function ($resource) {
       return $resource(config.backend.url + 'bills/:id', {}, {
               query: {method: 'GET', params: {}, isArray: true},
               update: {method: 'PUT', params: {id: '@id'}},
               save: {method: 'POST', params: {}}
           }
       );
   }
]);

services.factory('Tag', ['$resource',
   function ($resource) {
       return $resource(config.backend.url + 'tags/:tag_id', {}, {
               query: {method: 'GET', params: {}, isArray: false}
           }
       );
   }
]);

services.factory('Payee', ['$resource',
   function ($resource) {
       return $resource(config.backend.url + 'payees/:payee_id', {}, {
               query: {method: 'GET', params: {}, isArray: true}
           }
       );
   }
]);
