from checks.core import serializers
from checks.core.forms import UserForm
from checks.core.models import Bill, Payee, Tag, BillItem
from django.contrib.auth.models import User
from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticatedOrReadOnly


class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all().order_by('short_name')
    serializer_class = serializers.FullTagSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        user = self.request.user
        return Tag.objects.filter(user=user)


class PayeeViewSet(viewsets.ModelViewSet):
    queryset = Payee.objects.all().order_by('short_name')
    serializer_class = serializers.FullPayeeSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        user = self.request.user
        return Payee.objects.filter(user=user)


class BillViewSet(viewsets.ModelViewSet):
    queryset = Bill.objects.all().order_by('-date_time')
    serializer_class = serializers.BillDetailedSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        user = self.request.user
        return Bill.objects.filter(user=user).order_by('-date_time')


class BiilItemViewSet(viewsets.ModelViewSet):
    queryset = BillItem.objects.all()
    serializer_class = serializers.BillItemSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


def get_user(request):
    form = UserForm()
    return render(request, 'user.html', {'form': form})
