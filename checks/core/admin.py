from django.contrib import admin
from checks.core.models import Payee, BillItem, Bill, Tag, Wallet


class WithUserModelAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if getattr(obj, 'user', None) is None:
            obj.user = request.user
        super().save_model(request, obj, form, change)


class BillItemInline(admin.TabularInline):
    model = BillItem
    fields = ['description', 'tags', 'amount']
    extra = 3


class BillAdmin(WithUserModelAdmin):
    fields = ['date_time', 'payee', 'wallet', 'amount', 'adjustment', 'comment']
    inlines = [BillItemInline]

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'user', None) is None:
            obj.user = request.user
        super().save_model(request, obj, form, change)


# Register your models here.
admin.site.register(Payee, WithUserModelAdmin)
admin.site.register(Bill, BillAdmin)
admin.site.register(BillItem)
admin.site.register(Wallet, WithUserModelAdmin)
admin.site.register(Tag, WithUserModelAdmin)
