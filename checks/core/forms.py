from django import forms


class UserForm(forms.Form):
    name = forms.CharField(max_length=30)
    cc_myself = forms.BooleanField(required=False)
