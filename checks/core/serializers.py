from checks.core.models import Bill, Payee, Tag, BillItem
from django.contrib.auth.models import User
from rest_framework import serializers


class FullTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = '__all__'


class ShortTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'short_name')


class FullPayeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payee
        fields = '__all__'


class ShortPayeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'short_name')


class BillItemSerializer(serializers.ModelSerializer):
    # tags = ShortTagSerializer(many=True)

    class Meta:
        model = BillItem
        fields = '__all__'
        # extra_kwargs = {'id': {'read_only': False, 'required': False}}


class LessBillSerializer(serializers.ModelSerializer):
    payee = ShortPayeeSerializer()

    class Meta:
        model = Bill
        fields = ('id', 'date_time', 'payee', 'amount')


class BillDetailedSerializer(serializers.ModelSerializer):
    items = BillItemSerializer(many=True)

    # payee = ShortPayeeSerializer()

    class Meta:
        model = Bill
        fields = '__all__'

    def create(self, validated_data):
        print('=== created')
        items_data = validated_data.pop('items')
        bill = Bill.objects.create(**validated_data)
        i = 0
        for item_data in items_data:
            if 'in_bill_number' in item_data:
                del item_data['in_bill_number']
            tags = item_data.pop('tags', [])
            item = BillItem.objects.create(bill=bill, in_bill_number=i, **item_data)
            item.tags.add(*tags)
            i += 1
        return bill

    def update(self, instance, validated_data):
        print('=== updated')
        print(validated_data)
        instance.date_time = validated_data.get('date_time', instance.date_time)
        instance.payee = validated_data.get('payee', instance.payee)
        instance.amount = validated_data.get('amount', instance.amount)
        instance.adjustment = validated_data.get('adjustment', instance.adjustment)
        instance.comment = validated_data.get('comment', instance.comment)

        instance.save()

        for i in instance.items.all():
            i.delete()

        items_data = validated_data.pop('items')
        i = 0
        for item_data in items_data:
            if 'in_bill_number' in item_data:
                del item_data['in_bill_number']
            tags = item_data.pop('tags', [])
            item = BillItem.objects.create(bill=instance, in_bill_number=i, **item_data)
            item.tags.add(*tags)
            i += 1

        if items_data:
            items_serializer = BillItemSerializer(data=validated_data.get('items'), many=True)
            if items_serializer.is_valid(raise_exception=True):
                items_serializer.save(bill=instance)

            item_ids = [item.get('id') for item in validated_data['items']]
            for item in instance.items.all():
                if item.id not in item_ids:
                    item.delete()

            for i, item_data in enumerate(validated_data['items']):
                if 'in_bill_number' in item_data:
                    del item_data['in_bill_number']
                tags = item_data.pop('tags', [])
                item, _ = BillItem.get_or_create(id=item_data.get('id'))
                item.bill = instance
                item.in_bill_number = i
                item.amount = item_data.get('amount', item.amount)
                item.description = item_data.get('description', item.description)
                item.comment = item_data.get('comment', item.comment)
                item.save()
                item.tags.add(*tags)

        return instance


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'
