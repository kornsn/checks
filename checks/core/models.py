from uuid import uuid4

from django.contrib.auth.models import User
from django.db import models


def _new_uid():
    return uuid4().hex


class EntityBase(models.Model):
    id = models.CharField(max_length=255, primary_key=True, default=_new_uid, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class WithComment(models.Model):
    comment = models.TextField(blank=True)

    class Meta:
        abstract = True


class WithShortName(models.Model):
    short_name = models.CharField(max_length=30, unique=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.short_name


class WithDescription(models.Model):
    description = models.TextField(blank=True)

    class Meta:
        abstract = True


class OwnedByUser(models.Model):
    user = models.ForeignKey(User, editable=False)

    class Meta:
        abstract = True


class Wallet(EntityBase, WithShortName, WithDescription, WithComment, OwnedByUser):
    pass


class Tag(EntityBase, WithShortName, WithDescription, OwnedByUser):
    pass


class Payee(EntityBase, WithShortName, WithDescription, OwnedByUser):
    pass


class Bill(EntityBase, WithComment, OwnedByUser):
    date_time = models.DateTimeField()
    payee = models.ForeignKey(Payee, null=True)
    wallet = models.ForeignKey(Wallet)
    amount = models.DecimalField(max_digits=20, decimal_places=2)
    adjustment = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    def __str__(self):
        return "%s - %s - %s - %s" % (self.date_time, self.payee, self.amount, self.wallets)


class BillItem(EntityBase, WithDescription, WithComment):
    bill = models.ForeignKey(Bill, related_name='items', editable=False)
    in_bill_number = models.IntegerField(null=True)
    tags = models.ManyToManyField(Tag, blank=True)
    amount = models.DecimalField(max_digits=20, decimal_places=2)

    class Meta:
        ordering = ('in_bill_number', 'created_at')

    def __str__(self):
        return "%s - %s" % (self.amount, self.description)
